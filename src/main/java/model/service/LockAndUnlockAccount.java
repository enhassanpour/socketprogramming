package model.service;

import model.entity.Account;

import java.util.Optional;

public class LockAndUnlockAccount {
    private static LockAndUnlockAccount lockAndUnlockAccount = new LockAndUnlockAccount();

    public static LockAndUnlockAccount getInstance() {
        return lockAndUnlockAccount;
    }

    public Account getAccount(String custormerId) {
        try {
            DataService dataService = DataService.getInstance();
            Optional<Account> machingObject = dataService.accountList.stream().filter(p -> p.getCustomerId().equals(custormerId)).findFirst();
            Account account = machingObject.get();

            if (account.isLockForSync())
                return null;
            else {
                account.setLockForSync(true);
                return account;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void setAccountUnlock(String custormerId) {
        try {
            DataService dataService = DataService.getInstance();
            Optional<Account> machingObject = dataService.accountList.stream().filter(p -> p.getCustomerId().equals(custormerId)).findFirst();
            Account account = machingObject.get();
            account.setLockForSync(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
