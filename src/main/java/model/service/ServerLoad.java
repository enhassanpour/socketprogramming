package model.service;

import controller.AccountController;
import model.entity.Account;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServerLoad {
    public ServerLoad() {
    }

    private static ServerLoad serverLoad = new ServerLoad();

    public static ServerLoad getInstance() {
        return serverLoad;
    }

    public static void serverConfigAndRun() {
        List<Object> objectList = loadDataFromJson();
        DataService dataService = DataService.getInstance();
        dataService.accountList = (List<Account>) objectList.get(0);
        int port = (Integer) objectList.get(1);
        String logAddress = (String) objectList.get(2);
        System.setProperty("logfile.name", System.getProperty("user.dir") + "//src//main//resources//" + logAddress);
        RscodeService rscodeService = RscodeService.getInstance();
        multiSocketRun(port);
    }

    private static List<Object> loadDataFromJson() {
        JSONParser parser = new JSONParser();
        List<Object> resultList = null;
        try {
            Object obj = parser.parse(new FileReader(System.getProperty("user.dir") + "\\src\\main\\resources\\core.json"));
            JSONObject jsonObject = (JSONObject) obj;
            final int port = Integer.valueOf(jsonObject.get("port").toString());
            final String outLogAddress = (String) jsonObject.get("outLog").toString();
            JSONArray accountJsons = (JSONArray) jsonObject.get("deposits");
            Iterator<JSONObject> accountIterator = accountJsons.iterator();
            List<Account> accountList = new ArrayList<Account>();

            while (accountIterator.hasNext()) {
                Account account = new Account();
                jsonObject = accountIterator.next();
                account.setCustomerId(jsonObject.get("id").toString());
                account.setCustomerName(jsonObject.get("customer").toString());
                account.setInitialBalance(Long.valueOf(jsonObject.get("initialBalance").toString()));
                account.setUpperBound(Long.valueOf(jsonObject.get("upperBound").toString()));
                accountList.add(account);
            }

            resultList = new ArrayList<Object>();
            resultList.add(accountList);
            resultList.add(port);
            resultList.add(outLogAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }

    private static void multiSocketRun(int port) {
        Logger logger = Logger.getLogger(ServerLoad.class);
        try {
            ServerSocket serverSocket = null;
            serverSocket = new ServerSocket(port);
            System.out.println("start server");
            logger.debug("start server");

            while (true) {
                System.out.println("wait for clint");
                logger.info("wait for clint");
                Socket socket = serverSocket.accept();
                AccountController accountController = new AccountController(socket);
                Thread thread = new Thread(accountController);
                thread.start();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            logger.debug("start server");
        }
    }

}
