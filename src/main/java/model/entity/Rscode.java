package model.entity;

public enum Rscode {
    done,
    inUse,
    invalidInput,
    invalidUpperBound,
    negativeAmount,
    sentData
}
