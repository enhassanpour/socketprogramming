package model.entity;

public class Account {
    private String customerName;
    private String customerId;
    private long initialBalance;
    private long upperBound;
    private long balanceAccount;
    private boolean lockForSync;

    public boolean isLockForSync() {
        return lockForSync;
    }

    public Account setLockForSync(boolean lockForSync) {
        this.lockForSync = lockForSync;
        return this;
    }

    public long getBalanceAccount() {
        return balanceAccount;
    }

    public Account setBalanceAccount(long balanceAccount) {
        this.balanceAccount = balanceAccount;
        return this;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Account setCustomerName(String customerName) {
        this.customerName = customerName;
        return this;
    }

    public String getCustomerId() {
        return customerId;
    }

    public Account setCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public long getInitialBalance() {
        return initialBalance;
    }

    public Account setInitialBalance(long initialBalance) {
        this.initialBalance = initialBalance;
        return this;
    }

    public long getUpperBound() {
        return upperBound;
    }

    public Account setUpperBound(long upperBound) {
        this.upperBound = upperBound;
        return this;
    }
}
