package model.Business;

import model.entity.*;

import model.service.LockAndUnlockAccount;
import model.service.RscodeService;
import org.apache.log4j.Logger;

public class AccountBis {
    private RscodeService rscodeService;
    private LockAndUnlockAccount lockAndUnlockAccount;

    public AccountBis() {
        lockAndUnlockAccount = LockAndUnlockAccount.getInstance();
        rscodeService = RscodeService.getInstance();
    }

    private static Logger logger = Logger.getLogger(AccountBis.class);

    public RequestBody deposite(RequestBody requestBody)// return T = activity done; return F acctivity reject
    {
        RscodeValue rscodeValue;
        Account account = lockAndUnlockAccount.getAccount(requestBody.getCustomerId());
        try {

            if (account != null) {
                logger.debug("Start deposite by: " + requestBody.getTerminalId() + "on customer Id : " + requestBody.getCustomerId());

                if (requestBody.getAmount() > 0) {
                    account.setInitialBalance(account.getInitialBalance() + requestBody.getAmount());
                    rscodeValue = rscodeService.getRscodeValue(Rscode.done);
                    logger.info("Successfully deposite amount : " + requestBody.getAmount() + rscodeValue.toString());
                } else {
                    rscodeValue = rscodeService.getRscodeValue(Rscode.negativeAmount);
                    logger.error("unsuccessfull deposite amount : " + requestBody.getAmount() + rscodeValue.toString());
                }
            } else {
                rscodeValue = rscodeService.getRscodeValue(Rscode.inUse);
                logger.error("unsuccessfull withdraw amount : " + requestBody.getAmount() + rscodeValue.toString());
            }

            requestBody.setRscode(rscodeValue.getRscode());
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (account != null) lockAndUnlockAccount.setAccountUnlock(requestBody.getCustomerId());
            logger.debug("End  withdraw by: " + requestBody + "on customer Id : " + requestBody.getCustomerId());
            return requestBody;
        }

    }

    public RequestBody withdraw(RequestBody requestBody) {
        RscodeValue rscodeValue;
        Account account = lockAndUnlockAccount.getAccount(requestBody.getCustomerId());
        try {

            if (account != null) {
                logger.debug("Start withdraw by: " + requestBody.getTerminalId() + "on customer Id : " + requestBody.getCustomerId());

                if (account.getUpperBound() > requestBody.getAmount()) {

                    if (account.getInitialBalance() - requestBody.getAmount() > 0) {
                        account.setInitialBalance(account.getInitialBalance() - requestBody.getAmount());
                        rscodeValue = rscodeService.getRscodeValue(Rscode.done);
                        logger.info("Successfully withdraw amount : " + requestBody.getAmount() + rscodeValue.toString());
                    } else {
                        rscodeValue = rscodeService.getRscodeValue(Rscode.negativeAmount);
                        logger.error("unsuccessfull withdraw amount : " + requestBody.getAmount() + rscodeValue.toString());
                    }
                } else {
                    rscodeValue = rscodeService.getRscodeValue(Rscode.invalidUpperBound);
                    logger.error("unsuccessfull withdraw amount : " + requestBody.getAmount() + rscodeValue.toString());
                }
            } else {
                rscodeValue = rscodeService.getRscodeValue(Rscode.inUse);
                logger.error("unsuccessfull withdraw amount : " + requestBody.getAmount() + rscodeValue.toString());
            }

            requestBody.setRscode(rscodeValue.getRscode());
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (account != null) lockAndUnlockAccount.setAccountUnlock(requestBody.getCustomerId());
            logger.debug("End  withdraw by: " + requestBody + "on customer Id : " + requestBody.getCustomerId());
            return requestBody;
        }
    }
}
