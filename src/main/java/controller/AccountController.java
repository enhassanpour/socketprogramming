package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Business.AccountBis;
import model.entity.RequestBody;
import model.entity.RequestType;
import model.entity.Rscode;
import model.entity.RscodeValue;
import model.service.RscodeService;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class AccountController implements Runnable {

    Socket socket ;

    public AccountController(Socket socket) {
        this.socket = socket;
    }

    private static Logger logger = Logger.getLogger(AccountBis.class);

    @Override
    public void run() {
        try {
            DataInputStream dataInputStream =null;
            DataOutputStream dataOutputStream =null;
            RscodeService rscodeService = RscodeService.getInstance();
            ObjectMapper objectMapper = new ObjectMapper();
            logger.debug("accept request of one terminal  : ");
            System.out.println("Client accept");
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            AccountBis accountBis = new AccountBis();
            String datainput;

            while (true) {
                datainput = dataInputStream.readUTF();
                RequestBody requestBody = new RequestBody();
                requestBody = objectMapper.readValue(datainput, RequestBody.class);
                logger.info("Request Client received from terminal Id :" + requestBody.getTerminalId());
                System.out.println("Request Client received from terminal Id :" + requestBody.getTerminalId());

                if (requestBody.getAmount() > 0) {

                    if (requestBody.getRequestType() == RequestType.deposit) {
                        dataOutputStream.writeUTF(objectMapper.writeValueAsString(accountBis.deposite(requestBody)));
                    } else if (requestBody.getRequestType() == RequestType.withdraw) {
                        dataOutputStream.writeUTF(objectMapper.writeValueAsString(accountBis.withdraw(requestBody)));
                    }
                    logger.info("Request Client received from terminal Id :" + requestBody.getTerminalId() + " answerd");
                    System.out.println("Request Client received from terminal Id :" + requestBody.getTerminalId() + " answerd");
                } else {
                    requestBody.setRscode(Rscode.invalidInput);
                    RscodeValue rscodeValue=rscodeService.getRscodeValue(Rscode.invalidInput);
                    logger.error("Request Client received from terminal Id :" + requestBody.getTerminalId() + "had error :" + rscodeValue.toString());
                    System.out.println("Request Client received from terminal Id :" + requestBody.getTerminalId() + "had error :" +rscodeValue.toString());
                    dataOutputStream.writeUTF(requestBody.toString());
                }

                dataOutputStream.flush();
                logger.debug("end  request of one terminal ");
                System.out.println("end  request of one terminal ");
            }

        } catch (Exception e) {

            if (e.getMessage() == null) System.out.println("Client Disconnect");
            else
                e.printStackTrace();

        }
    }
}
