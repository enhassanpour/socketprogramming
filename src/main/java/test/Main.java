package test;

import model.service.ServerLoad;

import java.net.Socket;

public class Main {
    private Socket socket = null;

    public static void main(String[] args) {
        ServerLoad serverLoad = ServerLoad.getInstance();
        serverLoad.serverConfigAndRun();
    }

}
